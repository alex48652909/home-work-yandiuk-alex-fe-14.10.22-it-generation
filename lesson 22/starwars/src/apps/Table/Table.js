import React from "react";

import "./table.css"

const Table = ({ mas }) => {
    return (
        <>
            {Array.isArray(mas.results) ?
                mas.results.map((el) => {
                    const { name,
                        image,
                        height,
                        mass,
                        hair_color,
                        skin_color,
                        eye_color,
                        birth_year,
                        gender } = el;

                    return (

                        <div className="mains" key={Math.random() * 100}>
                            <div className="line">
                                <ul className="card">
                                   <li><img className="photo" src={image} alt="photo" /></li> 
                                    <li className="line__fone" >{name}</li>
                                    <li className="line__fone">{height}</li>
                                    <li className="line__fone">{mass}</li>
                                    <li className="line__fone"> {hair_color}</li>
                                    <li className="line__fone"> {skin_color}</li>
                                    <li className="line__fone"> {eye_color}</li>
                                    <li className="line__fone"> {birth_year}</li>
                                    <li className="line__fone"> {gender}</li>
                                    <li className="line__fone ">{el.url}</li>
                                </ul>
                            </div>
                        </div>
                    )
                })
                : <div className="error">Помилка</div>}
                </>)
}

export default Table