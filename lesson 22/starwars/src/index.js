import React from "react";
import ReactDOM from "react-dom";
import App from "./apps/app/app";

ReactDOM.render(<App />, document.querySelector('.body'));