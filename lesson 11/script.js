
document.write("First task" + "<br>");

var operand1 = +prompt("Enter first number");
var operand2 = +prompt("Enter second number");
var sign = prompt("Enter sign");

function calculate(operand1, operand2, sign) {
    if (isNaN(operand1) || isNaN(operand2) || operand1 == "" || operand2 == "" || operand1 == null || operand2 == null) {
        document.write("You enter mistake in number");
    } else if (sign != "+" && sign != "-" && sign != "/" && sign != "*") {
        document.write("You enter mistake in sign");
    } else {

        if (sign == "+") {
            var result = operand1 + operand2;
            document.write(result);
        };
        if (sign == "-") {
            var result = operand1 - operand2;
            document.write(result);
        };
        if (sign == "*") {
            var result = operand1 * operand2;
            document.write(result);
        };
        if (sign == "/") {
            var result = operand1 / operand2;
            document.write(result);
        };
    };
};
calculate(operand1, operand2, sign);


document.write("Second task" + "<br>");
let n = prompt(" Enter number");
function ret() {

    let a = 1;
    let b = 1;
    let c = 0;
    for (let index = 3; index <= n; index++) {

        c = a + b;
        a = b;
        b = c;
    };
    return c;
};
document.write(ret());


document.write("Third task" + "<br>");


function game() {
    var color= '#f80000';
    var wordUser = document.getElementById('result');
    var wordComp = document.getElementById ('result2');
    var userChoise = prompt("Enter your chose: rock, scissors or paper");
    
    if (userChoise != "rock" && userChoise != "scissors" && userChoise != "paper") {
        alert("You enter incorrect value")
    } else {
        var computerChoise = Math.round( Math.random()*3) ;
        if (computerChoise < 1) {
            computerChoise = "rock";
            alert("Computer choise rock")
          
        } else if (1 < computerChoise && computerChoise < 3) {
            computerChoise = "scissors";
            alert("Computer choise scissors")

        } else {
            computerChoise = "paper";
            alert("Computer choise paper")
        };

        if ((userChoise == "rock" && computerChoise == "scissors") || (userChoise == "scissors" && computerChoise == "paper") || (userChoise == "paper" && computerChoise == "rock")) {
            alert("User win");
            wordUser.style.color=color;
        } else if ((computerChoise == "rock" && userChoise == "scissors") || (computerChoise == "scissors" && userChoise == "paper") || (computerChoise == "paper" && userChoise == "rock")) {
            wordComp.style.color=color;
            alert("Computer win")
        } else { alert("Draw") };
    }

}
