import React from "react";
import ReactDOM from "react-dom";
import data from "./data";

function App() {
    const tableStyle = {
        border: "1px solid black",
        borderCollapse: "collapse",
        textAlign: "center"
       
    }

    return (
        <div>
            <h1>Курс валют</h1>
            <table style={tableStyle}>
                <thead >
                    <tr>
                        <th style={tableStyle}>Назва валюти</th>
                        <th>Курс валюти</th>
                    </tr>

                </thead>
                <tbody >

                    {data.map((el) => {
                        return (
                            <tr style={tableStyle} ><td style={tableStyle}>{el.rate}</td ><td style={tableStyle}>{el.txt}</td></tr>
                        );

                    })}
                </tbody>
            </table>
        </div>

    );
}

ReactDOM.render(<App />, document.getElementById('body'));

